package com.example.polomin.mono.UI;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.polomin.mono.R;

/**
 * Created by Polomin on 15.04.2016.
 */
public class PlayCollectionPagerAdapter extends FragmentStatePagerAdapter {
    public PlayCollectionPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position)
        {
            case 0:
                fragment = new TweaksFragment();
                Bundle args = new Bundle();
                // bla-bla-bla
                return fragment;
            case 1:
                fragment = new PlaybackFragment();
                return fragment;
            case 2:
                fragment = new PlaylistFragment();
                return fragment;
        }
        return new Fragment();
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position)
        {
            case 0:
                return "Tweaks";
        }
        return super.getPageTitle(position);
    }
}
