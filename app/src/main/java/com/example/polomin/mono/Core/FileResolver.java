package com.example.polomin.mono.Core;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by Polomin on 21.03.2016.
 */
public class FileResolver
{
    public static ArrayList<Song> getAllSongs(final ContentResolver musicResolver)
    {
        ArrayList<Song> res = new ArrayList<>();
        Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor cursor = musicResolver.query(musicUri, null, null, null, null);
        if(cursor != null && cursor.moveToFirst())
        {
            int titleColumn = cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.TITLE);
            int idColumn = cursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int artistColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int albumColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
            int albumIdColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
            do
            {
                long id = cursor.getLong(idColumn);
                String title = cursor.getString(titleColumn);
                String artist = cursor.getString(artistColumn);
                String album = cursor.getString(albumColumn);
                long artId = cursor.getLong(albumIdColumn);
                res.add(new Song(title, artist, album, id, artId));
            } while (cursor.moveToNext());
        }

        return res;
    }

    public static ArrayList<String> getAlbums(final ContentResolver musicResolver)
    {
        String[] projection = new String[] { MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM, MediaStore.Audio.Albums.ARTIST, MediaStore.Audio.Albums.ALBUM_ART, MediaStore.Audio.Albums.NUMBER_OF_SONGS };
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder = MediaStore.Audio.Media.ALBUM + " ASC";
        Cursor cursor = musicResolver.query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, /*projection*/null, selection, selectionArgs, sortOrder);
        ArrayList<String> res = new ArrayList<>();
        do
        {
            int albumColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
            String album = cursor.getString(albumColumn);
            res.add(album);
        }while (cursor.moveToNext());

        return res;
    }

    public static String getArtSrc(Activity v, String albumId)
    {
        Cursor cursor = v.managedQuery(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                new String[] {MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM_ART},
                MediaStore.Audio.Albums._ID+ "=?",
                new String[] {String.valueOf(albumId)},
                null);

        if (cursor.moveToFirst()) {
            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
            return path;
        }
        return "-1";
    }
}
