package com.example.polomin.mono.UI;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.polomin.mono.Core.Song;
import com.example.polomin.mono.PlayService;
import com.example.polomin.mono.R;

import java.util.ArrayList;

/**
 * Created by Polomin on 21.03.2016.
 */
public class SongsAdapter extends RecyclerView.Adapter<SongsAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView titleView;
        TextView artistView;
        CardView songCard;
        CheckBox songCheckbox;
        public ViewHolder(View itemView) {
            super(itemView );
            titleView = (TextView)itemView.findViewById(R.id.song_title);
            artistView = (TextView)itemView.findViewById(R.id.song_artist);
            songCard = (CardView)itemView.findViewById(R.id.song_card);
            songCheckbox = (CheckBox)itemView.findViewById(R.id.song_checkbox);
        }
    }

    private ArrayList<Song> songsList;
    private LayoutInflater inflater;
    private ArrayList<Song> songsToAdd = new ArrayList<>();
    private static Snackbar snack;

    public SongsAdapter(Context context, ArrayList<Song> songsList)
    {
        this.songsList = songsList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout songsLayout = (RelativeLayout)inflater.inflate(R.layout.song, parent, false);

        songsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayService playService = PlayService.getInstance(v.getContext());
                ArrayList<Song> songList;
                AppCompatActivity callerActivity = (AppCompatActivity)v.getContext();
                RecyclerView recycler;
                boolean flush = false;
                if(v.getContext() instanceof NavigationActivity) {
                    recycler = (RecyclerView) callerActivity.findViewById(R.id.songs_list);
                    songList = playService.getSongsList();
                    flush = true;
                }
                else {
                    recycler = (RecyclerView) callerActivity.findViewById(R.id.current_playlist_view);
                    songList = new ArrayList<>(playService.getQueue());
                }

                //TODO: нормальную очередь типа FileResolver.getAllSongsByGenreOf(song);
//                for (Song song : songList)
//                {

//                }

                if(playService.isPlaying())
                    playService.stop();

                int pos = recycler.getChildAdapterPosition(v);
                Song s = songList.get(pos);

                playService.enqueueSong(s);


                playService.play(0);

                if(flush) playService.flushQueue();

                if(v.getContext() instanceof NavigationActivity)
                        callerActivity.finish();
                else {
                    ((PlayActivity) callerActivity).setAlbumArt();
                    ((PlayActivity) callerActivity).invalidateControls();
                }
            }
        });

        ViewHolder vh = new ViewHolder(songsLayout);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        Song song = songsList.get(position);

        holder.titleView.setText(song.getTitle());
        holder.artistView.setText(song.getArtist());
        holder.songCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if(view.getContext() instanceof PlayActivity)
                {
                    return;
                }

                RecyclerView recycler = (RecyclerView)((NavigationActivity)(view.getContext())).findViewById(R.id.songs_list);
                int count = recycler.getChildCount();

                songsToAdd.clear();
                for(int i = 0; i < count; i++)
                {
                    View child = recycler.getChildAt(i);
                    CheckBox checkBox = (CheckBox) child.findViewById(R.id.song_checkbox);
                    if(checkBox.isChecked())
                        songsToAdd.add(songsList.get(recycler.getChildAdapterPosition(child)));

                }

                View.OnClickListener onAddToLibrary = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PlayService playService = PlayService.getInstance(v.getContext());
                        playService.flushQueue();
                        for(Song s : songsToAdd)
                        {
                            playService.enqueueSong(s);
                        }

                        update(new ArrayList<>(playService.getQueue()));
//                        ((NavigationActivity)v.getContext()).finish();
                    }
                };
                Snackbar sn = Snackbar.make(view, "Add selected songs to playlist", Snackbar.LENGTH_LONG)
                        .setAction("ADD", onAddToLibrary);
                if(snack == null || !snack.isShown()) {
                    sn.show();
                    snack = sn;
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        if(songsList.size() > 0)
            return songsList.get(position).getId();
        else return -1;
    }

    @Override
    public int getItemCount() {
        return songsList.size();
    }

    public void update(ArrayList<Song> list)
    {
        songsList = list;
        notifyDataSetChanged();
    }
}
