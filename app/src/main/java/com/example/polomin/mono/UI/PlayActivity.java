package com.example.polomin.mono.UI;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.polomin.mono.Core.Song;
import com.example.polomin.mono.PlayService;
import com.example.polomin.mono.R;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class PlayActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener {

    private PlayCollectionPagerAdapter playCollectionPagerAdapter;
    private ViewPager viewPager;
    private PlayService playService;
    private ImageView buttonPlayPause;
    private ImageView buttonNext;
    private ImageView buttonPrev;
    private SeekBar songProgress;
    private TextView startText;
    private TextView durationText;
    private static final long PROGRESS_UPDATE_INTERNAL = 1000;
    private static final long PROGRESS_UPDATE_INITIAL_INTERVAL = 100;
    private Drawable playDrawable;
    private Drawable pauseDrawable;
    private final ScheduledExecutorService executorService =
        Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> scheduleFuture;
    private final Handler handler = new Handler();

private final Runnable updateProgressBarTask = new Runnable() {
    @Override
    public void run() {
        int pos = playService.getPosition();
        if(songProgress == null) {
            setProgressView();
        }
        updateDuration(playService.getDuration());
        int progress = Math.round(pos / PROGRESS_UPDATE_INTERNAL);

        songProgress.setProgress(progress);
    }
};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setPager();

        setNavigationDrawer(toolbar);

        buttonPlayPause = (ImageView)findViewById(R.id.play_pause);
        buttonNext = (ImageView)findViewById(R.id.next);
        buttonPrev = (ImageView)findViewById(R.id.prev);
    }

    private void setProgressView() {
        songProgress = (SeekBar) findViewById(R.id.seekBar1);
        startText = (TextView) findViewById(R.id.startText);

        songProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                startText.setText(DateUtils.formatElapsedTime(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                stopSeekbarUpdate();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                playService.seek(seekBar.getProgress() * (int)PROGRESS_UPDATE_INTERNAL);
                scheduleSeekbarUpdate();
            }
        });
    }

    public void updateDuration(int duration)
    {
        if(durationText == null)
            durationText = (TextView) findViewById(R.id.endText);
        TextView line1 = (TextView) findViewById(R.id.line1);
        TextView line2 = (TextView) findViewById(R.id.line2);
        TextView line3 = (TextView) findViewById(R.id.line3) ;
        line1.setText(playService.getTitle());
        line2.setText(playService.getArtist());
        line3.setText(playService.getAlbum());
        durationText.setText(DateUtils.formatElapsedTime(duration / PROGRESS_UPDATE_INTERNAL));
    }

    private void setPager() {
        playCollectionPagerAdapter = new PlayCollectionPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager)findViewById(R.id.MainPager);
        viewPager.setAdapter(playCollectionPagerAdapter);
        viewPager.setOnPageChangeListener(this);
        viewPager.setCurrentItem(1);
    }

    @Override
    protected void onPostResume() {
        viewPager.setCurrentItem(1);
        updateCurrentPlaylist();
        super.onPostResume();
    }

    public void updateCurrentPlaylist() {
        RecyclerView playlistView = (RecyclerView)findViewById(R.id.current_playlist_view);
        if (playlistView == null || playService == null)
            return;
        SongsAdapter adapter = (SongsAdapter) playlistView.getAdapter();
        adapter.update(new ArrayList<>(playService.getQueue()));
        playlistView.invalidate();
    }

    private void setNavigationDrawer(Toolbar toolbar) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, PlayService.class);
        startService(intent);
        playService = PlayService.getInstance(this);
    }

    public void onPreviousClick(View v)
    {
        if (playService == null) return; // TODO: страховка
        playService.previous();
        invalidateControls();
        setAlbumArt();
    }

    public void onNextClick(View v)
    {
        if (playService == null) return; // TODO: страховка
        playService.next();
        setAlbumArt();
        invalidateControls();
    }

    public void onPlayClick(View v)
    {
        if (playService == null) return;
        if(playService.isPlaying()) {
            stopSeekbarUpdate();
            playService.pause();
        }
        else {
            scheduleSeekbarUpdate();
            playService.play(playService.getCurrentIndex());
            updateDuration(playService.getDuration());
        }
        setBandsValue();
        setAlbumArt();
        invalidateControls();
    }

    @Override
    protected void onPause() {
        stopSeekbarUpdate();
        super.onPause();
    }

    public void setAlbumArt() {
        if(playService == null) return;
        String path = playService.getArtPath(this);
        Drawable art;
        if(path!= null && path != "-1")
            art = Drawable.createFromPath(path);
        else art = getResources().getDrawable(R.drawable.cat);
        ImageView background = (ImageView) findViewById(R.id.background_image);
        background.setImageDrawable(art);
        background.setVisibility(View.VISIBLE);
    }

    public void onNavigationClick(View v)
    {
        Intent intent = new Intent(this, NavigationActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (playService != null && playService.isPlaying()) {
            scheduleSeekbarUpdate();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.nav_library:
                onNavLibraryCLick();
                break;
            case R.id.nav_settings:
                onNavSettingsClick();
                break;
            case R.id.nav_about:
                onNavAboutCLick();
                break;
            case R.id.nav_exit:
                onNavExitClick();
                break;
        }
        return false;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.action_library);
        if(position == 2)
            fab.show();
        else fab.hide();
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void invalidateControls()
    {
        if(playService == null) return;
        playDrawable = ContextCompat.getDrawable(this, R.drawable.ic_play_arrow_white_24dp);
        pauseDrawable = ContextCompat.getDrawable(this, R.drawable.ic_pause_white_24dp);
        if(buttonPlayPause == null) buttonPlayPause = (ImageView)findViewById(R.id.play_pause);

        if(playService.isPlaying())
            buttonPlayPause.setImageDrawable(pauseDrawable);
        else
            buttonPlayPause.setImageDrawable(playDrawable);
    }

    public void scheduleSeekbarUpdate() {
        stopSeekbarUpdate();
        if (!executorService.isShutdown()) {
            scheduleFuture = executorService.scheduleAtFixedRate(
                    new Runnable() {
                        @Override
                        public void run() {
                            handler.post(updateProgressBarTask);
                        }
                    }, PROGRESS_UPDATE_INITIAL_INTERVAL,
                    PROGRESS_UPDATE_INTERNAL, TimeUnit.MILLISECONDS);
        }
    }

    private void stopSeekbarUpdate() {
        if (scheduleFuture != null) {
            scheduleFuture.cancel(false);
        }
    }

    public void onNavLibraryCLick()
    {
        Intent intent = new Intent(this, NavigationActivity.class);
        startActivity(intent);
    }

    public void onNavSettingsClick()
    {

    }

    public void onNavAboutCLick()
    {

    }

    public void onNavExitClick()
    {
        if(PlayService.getInstance(this) != null) {
            PlayService.getInstance(this).stop();
            stopService(new Intent(this, PlayService.class));
        }

        finish();
    }

    public void setBandsValue()
    {
        if(playService == null) return;

        SeekBar band1 = (SeekBar)findViewById(R.id.seek_freq_1);
        SeekBar band2 = (SeekBar)findViewById(R.id.seek_freq_2);
        SeekBar band3 = (SeekBar)findViewById(R.id.seek_freq_3);
        SeekBar band4 = (SeekBar)findViewById(R.id.seek_freq_4);
        SeekBar band5 = (SeekBar)findViewById(R.id.seek_freq_5);

        band1.setProgress(playService.getEqualizerBandLevel(0));
        band2.setProgress(playService.getEqualizerBandLevel(1));
        band3.setProgress(playService.getEqualizerBandLevel(2));
        band4.setProgress(playService.getEqualizerBandLevel(3));
        band5.setProgress(playService.getEqualizerBandLevel(4));
    }
}
