package com.example.polomin.mono.Core;

/**
 * Created by Polomin on 12.02.2016.
 */
public class Song {
    /*private String filename;*/

    private String title;
    private String artist;
    private String album;
    private long id = -1;
    private long artId = -1;

    public Song(String title, String artist, String album, long id, long artId)
    {
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.artId = artId;
        setId(id);
    }
    public long getId()
    {
        return id;
    }

    private void setId(long id)
    {
        // TODO: запилить проверку на пересечения с уже существующим id
        this.id = id;
    }

    public String getAlbum()
    {
        return album;
    }

    public long getArtId() {
        return artId;
    }

    public void setAlbum(String album)
    {
        this.album = album;
    }

    public String getArtist()
    {
        return artist;
    }

    public void setArtist(String artist)
    {
        this.artist = artist;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    /*public String getFileName()
    {
        return filename;
    }*/
}
