package com.example.polomin.mono.UI;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.polomin.mono.PlayService;
import com.example.polomin.mono.R;

/**
 * Created by Polomin on 16.04.2016.
 */
public class TweaksFragment extends Fragment {

    private TextView freq1;
    private TextView freq2;
    private TextView freq3;
    private TextView freq4;
    private TextView freq5;

    private SeekBar band1;
    private SeekBar band2;
    private SeekBar band3;
    private SeekBar band4;
    private SeekBar band5;

    private CheckBox bassCheckBox;
    private CheckBox nsCheckBox;

    @Override
    public void onResume() {
        super.onResume();
        ((PlayActivity)getActivity()).setBandsValue();
    }

    private static final int EQ_OFFSET = 1500;
    private static final int EQ_MAX = 3000;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tweaks_fragment, container, false);
        band1 = (SeekBar)rootView.findViewById(R.id.seek_freq_1);
        band2 = (SeekBar)rootView.findViewById(R.id.seek_freq_2);
        band3 = (SeekBar)rootView.findViewById(R.id.seek_freq_3);
        band4 = (SeekBar)rootView.findViewById(R.id.seek_freq_4);
        band5 = (SeekBar)rootView.findViewById(R.id.seek_freq_5);

        band1.setOnSeekBarChangeListener(new BandSeekListener());
        band1.setMax(EQ_MAX);
        band2.setOnSeekBarChangeListener(new BandSeekListener());
        band2.setMax(EQ_MAX);
        band3.setOnSeekBarChangeListener(new BandSeekListener());
        band3.setMax(EQ_MAX);
        band4.setOnSeekBarChangeListener(new BandSeekListener());
        band4.setMax(EQ_MAX);
        band5.setOnSeekBarChangeListener(new BandSeekListener());
        band5.setMax(EQ_MAX);

        return rootView;
    }

    private class BandSeekListener implements SeekBar.OnSeekBarChangeListener
    {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            int bandNum = -1;
            int seekTo = seekBar.getProgress();
            switch (seekBar.getId())
            {
                case R.id.seek_freq_1:
                    bandNum = 0;
                    break;
                case R.id.seek_freq_2:
                    bandNum = 1;
                    break;
                case R.id.seek_freq_3:
                    bandNum = 2;
                    break;
                case R.id.seek_freq_4:
                    bandNum = 3;
                    break;
                case R.id.seek_freq_5:
                    bandNum = 4;
                    break;
            }

            // impossible case but why the hell not
            if(bandNum == -1) return;

            PlayService playService = PlayService.getInstance(seekBar.getContext());
            if(playService != null)
                playService.setEqualizerBandLevel(bandNum, seekTo - EQ_OFFSET);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }
}
