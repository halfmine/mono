package com.example.polomin.mono.UI;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.polomin.mono.Core.FileResolver;
import com.example.polomin.mono.Core.Player;
import com.example.polomin.mono.Core.Song;
import com.example.polomin.mono.PlayService;
import com.example.polomin.mono.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class NavigationActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener
{
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        LibraryCollectionPagerAdapter adapter = new LibraryCollectionPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager)findViewById(R.id.navigation_pager);
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(this);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
