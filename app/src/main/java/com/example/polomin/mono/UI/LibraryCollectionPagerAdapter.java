package com.example.polomin.mono.UI;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Polomin on 23.04.2016.
 */
public class LibraryCollectionPagerAdapter extends FragmentStatePagerAdapter {
    public LibraryCollectionPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    private static final int FRAGMENT_ALL = 0;
    private static final int FRAGMENT_GENRES = 1;
    private static final int FRAGMENT_ALBUMS = 2;

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position)
        {
            case FRAGMENT_ALL:
                return "All";
        }
        return super.getPageTitle(position);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case FRAGMENT_ALL:
                return new AllSongsFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 1;
    }
}
