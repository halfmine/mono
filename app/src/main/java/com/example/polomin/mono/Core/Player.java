package com.example.polomin.mono.Core;

import android.content.ContentUris;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.Equalizer;
import android.media.audiofx.NoiseSuppressor;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.MediaStore;

import com.example.polomin.mono.PlayService;

import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Polomin on 21.02.2016.
 */
public class Player {
    private static Player ourInstance = new Player();

    public static Player getInstance() {
        if(ourInstance == null) ourInstance = new Player();
        return ourInstance;
    }
    private MediaPlayer mediaPlayer;

    // this is for gapless playback
//    private MediaPlayer nextPlayer;

    private Equalizer equalizer;
    private NoiseSuppressor noiseSuppressor;

    private Equalizer.Settings eqSettings;

    private int songPosition = 0;

    private Player() {
        /*nextPlayer*/mediaPlayer = new MediaPlayer();
    }

    public void prepare(final Context context)
    {
        if(mediaPlayer == null) mediaPlayer = new MediaPlayer(); // TODO: БОООООООЛЬШЕ СТРАХОВОК!
        /*nextPlayer*/mediaPlayer.setOnPreparedListener(PlayService.getInstance(context));
        /*nextPlayer*/mediaPlayer.setOnCompletionListener(PlayService.getInstance(context));
        /*nextPlayer*/mediaPlayer.setOnErrorListener(PlayService.getInstance(context));
        /*nextPlayer*/mediaPlayer.setWakeMode(context, PowerManager.PARTIAL_WAKE_LOCK);
        /*nextPlayer*/mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    public enum State {Playing, Paused, Stopped}

    private Song currentSong;

    private LinkedList<Song> playQueue = new LinkedList<>();
    private State state = State.Stopped;

    public Song getCurrentSong()
    {
        return currentSong;
    }

    public void setCurrentSong(Song currentSong)
    {
        this.currentSong = currentSong;
    }
    public void play(final Context context, int number) throws IOException {
        int _number = number;
        if(state != State.Paused)
        {

            if(playQueue.size() < 1) return; // паттерн "страховка"

            dequeueSong(number);


            if(mediaPlayer == null || state == State.Stopped)
            {
                /*nextPlayer*/mediaPlayer = new MediaPlayer();
                /*nextPlayer*/mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                prepare(context);
            }

            Uri songUri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, currentSong.getId());
            /*nextPlayer*/mediaPlayer.reset();
            /*nextPlayer*/mediaPlayer.setDataSource(context, songUri);
            /*nextPlayer*/mediaPlayer.prepareAsync();

            if(mediaPlayer != null) {
                mediaPlayer.start();
                setState(State.Playing);
            }

            // if we had some eq with settings then try to create new eq with them
            if(equalizer != null) {
                eqSettings = equalizer.getProperties();
                equalizer.release();
            }

            setUpEqualizer();
        }
        else
        {
            mediaPlayer.start();
            setState(State.Playing);
        }
    }

    public void createPlayer(MediaPlayer mp, Context context)
    {
        mp = new MediaPlayer();
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        prepare(context);
    }

    public void play(final Context context) throws IOException
    {
        int index = playQueue.indexOf(currentSong);
        index = index == -1 ? 0 : index;
        play(context, index);
    }

    public int getCurrentIndex()
    {
        int index = playQueue.indexOf(currentSong);
        return index == -1 ? 0 : index;
    }

    public void stop()
    {
        state = State.Stopped;
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer = null;
    }

    public void pause()
    {
        state = State.Paused;
        mediaPlayer.pause();
    }

    public void dequeueSong(int index)
    {
        currentSong =  playQueue.get(index);
    }

    public void dequeueSong()
    {
        dequeueSong(0);
    }

    private Song getPrevious()
    {
        int index = playQueue.indexOf(currentSong);
        if(index != -1)
        {
            if (index - 1 < 0) return currentSong;
            Song prev = playQueue.get(index - 1);
            return prev;
        }

        return currentSong;
    }

    private Song getNext()
    {
        int index = playQueue.indexOf(currentSong);
        if(index != -1)
        {
            if (index + 1 > playQueue.size()) return currentSong;
            Song next = playQueue.get(index + 1);
            return next;
        }

        return currentSong;
    }

    public void next()
    {
        currentSong = getNext();
    }

    public void previous()
    {
        currentSong = getPrevious();
    }

    public void enqueueSong(Song s)
    {
        playQueue.add(s);
    }

    public void flushQueue()
    {
        playQueue.clear();
    }

    public void setState(State state)
    {
        this.state = state;
    }

    public State getState()
    {
        return state;
    }

    public int getSongPosition()
    {
        if(mediaPlayer == null) return 0;

        return mediaPlayer.getCurrentPosition();
    }

    public int getSongDuration()
    {
        if(mediaPlayer == null) return 0;

        return mediaPlayer.getDuration();
    }

    public void seek(int position)
    {
        if(mediaPlayer == null) return;
        mediaPlayer.seekTo(position);
    }

    public void setUpEqualizer()
    {
        equalizer = new Equalizer(0, /*nextPlayer*/mediaPlayer.getAudioSessionId());
        equalizer.setEnabled(true);

        // TODO: докрутить присобачивание настроек

        /*if(eqSettings != null)
            equalizer.setProperties(eqSettings);*/
    }

    public short getEqualizerBandLevel(int bandNum)
    {
        if(equalizer == null) return -1;

        return equalizer.getBandLevel((short)bandNum);
    }

    public int getEqualizerBandCenter(int bandNum)
    {
        if(equalizer == null) return -1;
        return equalizer.getCenterFreq((short)bandNum);
    }

    public void setEqualizerBandLevel(int bandNum, int level)
    {
        if(equalizer == null) return;
        equalizer.setBandLevel((short)bandNum, (short)level);
    }

    public boolean isEmptyQueue()
    {
        return playQueue.size() < 1;
    }

    public boolean setNoiseSuppressor()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            noiseSuppressor = NoiseSuppressor.create(mediaPlayer.getAudioSessionId());
            return true;
        }

        return false;
    }

    public void releaseSuppressor()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            noiseSuppressor.release();
        }
    }

    public void onPrepared(MediaPlayer mp)
    {
//        if(mediaPlayer == null) {
//            mediaPlayer = mp;
            /*mediaPlayer*/mp.start();
            setState(Player.State.Playing);
//        }
//        else
//            nextPlayer = mp;
    }

    public void onComplete(MediaPlayer mp, Context context)
    {
//        mediaPlayer = nextPlayer;
//        if(getNext() != currentSong)
//        {
//            next();
//            mediaPlayer.start();
//        }
    }

    public LinkedList<Song> getQueue()
    {
        return playQueue;
    }
}
