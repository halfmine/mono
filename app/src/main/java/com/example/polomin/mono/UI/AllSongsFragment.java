package com.example.polomin.mono.UI;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.polomin.mono.Core.Player;
import com.example.polomin.mono.Core.Song;
import com.example.polomin.mono.PlayService;
import com.example.polomin.mono.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Polomin on 23.04.2016.
 */
public class AllSongsFragment extends Fragment {
    private PlayService playService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.library_all_fragment, container, false);

        playService = PlayService.getInstance(rootView.getContext());

        ArrayList<Song> songList = playService.getSongsList();
        Collections.sort(songList, new Comparator<Song>()
        {
            @Override
            public int compare(Song lhs, Song rhs) {
                return lhs.getTitle().compareTo(rhs.getTitle());
            }

        });
        RecyclerView songsView = (RecyclerView)rootView.findViewById(R.id.songs_list);
        songsView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(rootView.getContext());
        songsView.setLayoutManager(mLayoutManager);

        SongsAdapter songsAdapter = new SongsAdapter(rootView.getContext(), songList);
        songsView.setAdapter(songsAdapter);

        return rootView;
    }
}
