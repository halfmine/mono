package com.example.polomin.mono;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.NotificationCompat;
import android.widget.Toast;
import android.os.Process;

import com.example.polomin.mono.Core.FileResolver;
import com.example.polomin.mono.Core.Player;
import com.example.polomin.mono.Core.Song;
import com.example.polomin.mono.UI.PlayActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

public class PlayService extends Service implements MediaPlayer.OnCompletionListener,
                                                    MediaPlayer.OnErrorListener,
                                                    MediaPlayer.OnPreparedListener,
                                                    AudioManager.OnAudioFocusChangeListener
{
    public PlayService()
    {
    }

    private static PlayService instance;
    private int startMode = START_NOT_STICKY;
    private Player player = Player.getInstance();
    Looper looper;

    private static final int NOTIFICATION_ID = 2;
    private static final int PREV_ID = 3;
    private static final int NEXT_ID = 4;
    private static final int START_PAUSE_ID = 5;
    private static final int EQ_OFFSET = 1500;

    public ArrayList<Song> getSongsList()
    {
        return songsList;
    }

    private ArrayList<Song> songsList = new ArrayList<>();

    @Override
    public void onCompletion(MediaPlayer mp) {
        // тут мф должны передать воспроизведение следующему MediaPlayer
        player.onComplete(mp, getApplicationContext());
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onAudioFocusChange(int focusChange) {

    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            Bundle b = intent.getExtras();
            if (b.containsKey("startPause"))
            {
                switch (player.getState())
                {
                    case Playing:
                        player.pause();
                        break;
                    case Paused:
                        try {
                            player.play(getApplicationContext());
                        } catch (IOException e) {
                            player.stop();
                        }
                        break;
                }
            }
            else if(b.containsKey("prev"))
            {
                previous();
            }
            else if(b.containsKey("next"))
            {
                next();
            }
        }
        catch (Exception e)
        {
        }
        return startMode;
    }

    @Override
    public void onCreate()
    {
        HandlerThread handlerThread = new HandlerThread("PlayService", Process.THREAD_PRIORITY_DEFAULT);
        handlerThread.start();
        looper = handlerThread.getLooper();
        instance = this;
        Toast.makeText(PlayService.this, "PlayService starting...", Toast.LENGTH_SHORT).show();
        super.onCreate();
        if (player == null)
            player = Player.getInstance();
        player.prepare(this);
        scanForMusic();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(PlayService.this, "PlayService dying...", Toast.LENGTH_SHORT).show();
        looper.quit();
        instance = null;
        super.onDestroy();
    }

    public static PlayService getInstance(Context context)
    {
        if(instance == null)
        {
            context.startService(new Intent(context, PlayService.class));
        }

        return instance;
    }

    public void play(int number) {
        try {

            player.play(getApplicationContext(), number);
            Song s = player.getCurrentSong();
            PendingIntent prevPendingIntent = PendingIntent.getService(this, PREV_ID, new Intent(this, PlayService.class).putExtra("prev", PREV_ID), PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent nextPendingIntent = PendingIntent.getService(this, NEXT_ID, new Intent(this, PlayService.class).putExtra("next", NEXT_ID), PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent startPausePendingIntent = PendingIntent.getService(this, START_PAUSE_ID, new Intent(this, PlayService.class).putExtra("startPause", START_PAUSE_ID).putExtra("Song", s.getTitle() + s.getId()), PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder mBuilder =
                    (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                            .setSmallIcon(android.R.drawable.ic_media_play)
                            .setContentTitle(s.getTitle() + s.getId())
                            .addAction(R.drawable.ic_skip_previous_white_24dp, "", prevPendingIntent)
                            .addAction(R.drawable.ic_pause_white_24dp, "", startPausePendingIntent)
                            .addAction(R.drawable.ic_skip_next_white_24dp, "", nextPendingIntent)
                            .setOngoing(true);
            Intent resultIntent = new Intent(this, PlayActivity.class);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

            stackBuilder.addParentStack(PlayActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(
                            0,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            mBuilder.setContentIntent(resultPendingIntent);
            Notification notification = mBuilder.build();
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(NOTIFICATION_ID, notification);

            startForeground(NOTIFICATION_ID, notification);
        }
        catch (IOException e)
        {
            player.stop();
            Toast.makeText(PlayService.this, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public void pause()
    {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(NOTIFICATION_ID);
        stopForeground(true);
        player.pause();
    }

    public void stop()
    {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(NOTIFICATION_ID);
        stopForeground(true);
        if (isPlaying()) player.pause();
        player.stop();
    }

    public int getDuration()
    {
        return player.getSongDuration();
    }

    public String getTitle()
    {
        return player.getCurrentSong().getTitle();
    }

    public String getAlbum()
    {
        return player.getCurrentSong().getAlbum();
    }

    public String getArtist()
    {
        return player.getCurrentSong().getArtist();
    }

    public int getPosition()
    {
        return Player.getInstance().getSongPosition();
    }

    public void seek(int position)
    {
        Player.getInstance().seek(position);
    }

    public boolean isPlaying()
    {
        if(player == null)
            return false;
        return player.getState() == Player.State.Playing;
    }

    public void scanForMusic()
    {
        // TODO: пока тупо добавляем все в очередь
        songsList = FileResolver.getAllSongs(getContentResolver());
    }

    public void enqueueSong(Song s)
    {
        player.enqueueSong(s);
    }

    public void flushQueue()
    {
        if(player != null) player.flushQueue();
    }

    public void previous()
    {
        player.previous();
        if(isPlaying())
            play(player.getCurrentIndex());
        else if (player.getState() != Player.State.Stopped)
        {
            stop();
        }
    }

    public void next()
    {
        player.next();
        if(isPlaying())
            play(player.getCurrentIndex());
        else if (player.getState() != Player.State.Stopped)
        {
            stop();
        }
    }

    public int getCurrentIndex()
    {
        return player.getCurrentIndex();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        player.onPrepared(mp);
    }

    public String getArtPath(Activity v)
    {
        String path = FileResolver.getArtSrc(v, String.valueOf(player.getCurrentSong().getArtId()));
        return path;
    }

    public int getEqualizerBandLevel(int bandNum)
    {
        return player.getEqualizerBandLevel(bandNum) + EQ_OFFSET;
    }

    public int getEqualizerBandCenter(int bandNum)
    {
        return player.getEqualizerBandCenter(bandNum);
    }

    public void setEqualizerBandLevel(int bandNum, int level)
    {
        player.setEqualizerBandLevel(bandNum, level - EQ_OFFSET);
    }

    public LinkedList<Song> getQueue()
    {
        if (player == null) return new LinkedList<>();
        return player.getQueue();
    }
}
