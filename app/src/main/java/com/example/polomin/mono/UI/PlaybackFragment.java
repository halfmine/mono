package com.example.polomin.mono.UI;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.polomin.mono.PlayService;
import com.example.polomin.mono.R;

/**
 * Created by Polomin on 15.04.2016.
 */
public class PlaybackFragment extends Fragment{
    public static final String ARG_OBJECT = "object";

    @Override
    public void onResume() {
        super.onResume();
        PlayService playService = PlayService.getInstance((getActivity()));
        if(playService != null && playService.isPlaying()) {
            ((PlayActivity)getActivity()).scheduleSeekbarUpdate();
            ((PlayActivity)getActivity()).updateDuration(playService.getDuration());
            ((PlayActivity)getActivity()).invalidateControls();
            ((PlayActivity)getActivity()).setAlbumArt();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.playback_fragment, container, false);
        PlayActivity playActivity = (PlayActivity)rootView.getContext();
        playActivity.invalidateControls();
        playActivity.setAlbumArt();
        return rootView;
    }
}
