package com.example.polomin.mono.UI;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.polomin.mono.Core.Song;
import com.example.polomin.mono.PlayService;
import com.example.polomin.mono.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

/**
 * Created by Polomin on 16.04.2016.
 */
public class PlaylistFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.playlist_fragment, container, false);

        PlayService playService = PlayService.getInstance(getContext());

        LinkedList<Song> songList = playService.getQueue();
        Collections.sort(songList, new Comparator<Song>()
        {
            @Override
            public int compare(Song lhs, Song rhs) {
                return lhs.getTitle().compareTo(rhs.getTitle());
            }

        });
        RecyclerView songsView = (RecyclerView)rootView.findViewById(R.id.current_playlist_view);
        songsView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        songsView.setLayoutManager(mLayoutManager);

        SongsAdapter songsAdapter = new SongsAdapter(getContext(), new ArrayList<>(songList));
        songsView.setAdapter(songsAdapter);


        return rootView;
    }

    @Override
    public void onResume() {
        ((PlayActivity)getActivity()).updateCurrentPlaylist();
        super.onResume();
    }
}
